#! /usr/bin/env python

import sys
import subprocess
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

"""
euc - euclidean metric
airm - airm metric
logd - log_determinant metric
loge - log_euclidean metric
"""

tests = 30
minp,maxp = 2,9

# stores variance retained indexed by p
eucpvar = []
airmpvar = []
logdpvar = []
logepvar = []

# stores eigenvalues retained indexed by p
eucpeig = []
airmpeig = []
logdpeig = []
logepeig = []

for p in xrange(minp,maxp+1):
    eucvar = euceig = 0
    airmvar = airmeig = 0
    logdvar = logdeig = 0
    logevar = logeeig = 0
    for test in xrange(tests):
        print p,test
        datapath = "Datasets/dataset%d.pickle"%(test+1)
        proc = subprocess.Popen(["python", "main.py", datapath, str(p)], stdout=subprocess.PIPE, shell=False)
        out,err = proc.communicate()
        euc,airm,logd,loge = out.strip().split('\n')
        var,eig = map(float,euc.split(' '))
        eucvar += var
        euceig += eig
        var,eig = map(float,airm.split(' '))
        airmvar += var
        airmeig += eig
        var,eig = map(float,logd.split(' '))
        logdvar += var
        logdeig += eig
        var,eig = map(float,loge.split(' '))
        logevar += var
        logeeig += eig
    eucpvar.append(eucvar/tests)
    eucpeig.append(euceig/tests)
    airmpvar.append(airmvar/tests)
    airmpeig.append(airmeig/tests)
    logdpvar.append(logdvar/tests)
    logdpeig.append(logdeig/tests)
    logepvar.append(logevar/tests)
    logepeig.append(logeeig/tests)

print eucpvar
print eucpeig
print airmpvar
print airmpeig
print logdpvar
print logdpeig
print logepvar
print logepeig

plt.figure(1)
r_patch = mpatches.Patch(color='red', label="Euclidean")
g_patch = mpatches.Patch(color='green', label="AIRM")
b_patch = mpatches.Patch(color='blue', label="Log Determinant")
br_patch = mpatches.Patch(color='black', label="Log Euclidean")
plt.legend(handles=[r_patch,g_patch,b_patch,br_patch],prop={'size':10})
plt.axis((1,9,0,0.7))
plt.xlabel("Target Dimension p", fontsize=16)
plt.ylabel("Variance Compression", fontsize=16)
plt.plot([i for i in xrange(minp,maxp+1)],eucpvar,"r^")
plt.plot([i for i in xrange(minp,maxp+1)],eucpvar,"r")
plt.plot([i for i in xrange(minp,maxp+1)],airmpvar,"g^")
plt.plot([i for i in xrange(minp,maxp+1)],airmpvar,"g")
plt.plot([i for i in xrange(minp,maxp+1)],logdpvar,"b^")
plt.plot([i for i in xrange(minp,maxp+1)],logdpvar,"b")
plt.plot([i for i in xrange(minp,maxp+1)],logepvar,"k^")
plt.plot([i for i in xrange(minp,maxp+1)],logepvar,"k")
plt.savefig("variance.png", bbox_inches='tight')

plt.figure(2)
r_patch = mpatches.Patch(color='red', label="Euclidean")
g_patch = mpatches.Patch(color='green', label="AIRM")
b_patch = mpatches.Patch(color='blue', label="Log Determinant")
br_patch = mpatches.Patch(color='black', label="Log Euclidean")
plt.legend(handles=[r_patch,g_patch,b_patch,br_patch],prop={'size':10},loc="upper left")
plt.axis((1,9,0,1.0))
plt.xlabel("Target Dimension p", fontsize=16)
plt.ylabel("Eigen values retained", fontsize=16)
plt.plot([i for i in xrange(minp,maxp+1)],eucpeig,"r^")
plt.plot([i for i in xrange(minp,maxp+1)],eucpeig,"r")
plt.plot([i for i in xrange(minp,maxp+1)],airmpeig,"g^")
plt.plot([i for i in xrange(minp,maxp+1)],airmpeig,"g")
plt.plot([i for i in xrange(minp,maxp+1)],logdpeig,"b^")
plt.plot([i for i in xrange(minp,maxp+1)],logdpeig,"b")
plt.plot([i for i in xrange(minp,maxp+1)],logepeig,"k^")
plt.plot([i for i in xrange(minp,maxp+1)],logepeig,"k")
plt.savefig("eigenstorage.png", bbox_inches='tight')
