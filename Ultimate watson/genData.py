#! /usr/bin/env python

from sklearn.datasets import make_spd_matrix as msm
import pickle
import numpy

for test in xrange(30):
    dim = 17
    numsamples = 50
    samples = []
    for i in xrange(numsamples):
        samples.append(numpy.array(msm(dim)))
    samples = numpy.array(samples)
    pickle.dump(samples,open("Datasets/dataset%d.pickle"%(test+1), "wb"))
