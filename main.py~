#! /usr/bin/env python

import pickle
import numpy
import numpy as np
import math
from numpy import linalg as LA

samples = pickle.load(open("dataset.pickle","rb"))
numsamples = samples.shape[0]
n = samples.shape[1] # Old Dimension
p = 5 # New Dimension

def cosine(s):
    # In expansion of cosine, I = p*p identity, A^2 = A.T*A, A^(2k) = (A^2)^k
    n,p = s.shape
    S = np.matrix([[0 if i!=j else math.cos(s[i,j]) for j in xrange(p)] for i in xrange(p) ])
    return S

def sine(s):
    # In expansion of sine A = A, A^3 = A*(A^2) = A*A.T*A, A^(2k+1) = A*((A^2)^k)
    n,p = s.shape
    S = np.matrix([[0 if i!=j else math.sin(s[i,j]) for j in xrange(p)] for i in xrange(n) ])
    return S

def update(W,H):
    U,s,V = np.linalg.svd(H)
    S = np.matrix([[0 if i!=j else s[j] for j in xrange(p)] for i in xrange(n) ]) # since s contains only eigen values make it diagonal
    """
    sin = sine(S)
    cos = cosine(S)
    print np.matmul(sin.T,sin) + np.matmul(cos.T,cos)
    """
    newW = np.matmul(np.matmul(W,V),np.matmul(cosine(S),V.T)) + np.matmul(U,np.matmul(sine(S),V.T))
    return newW

def variance(W):
    mean = samples.mean(0)
    pre_whiten(mean)
    var = 0
    for i in samples:
        # var+=np.trace(np.matmul(np.matmul(W.T,np.matmul(i,W)),np.matmul(W.T,np.matmul(mean,W))))
        var+=LA.norm(np.matmul(W.T,np.matmul(i,W))-np.matmul(W.T,np.matmul(mean,W)),None)
    var/=float(numsamples)
    print var
    return var
    
def matrix_log(matrix):
    v,u = np.linalg.eig(matrix)
    v = [np.log(x) for x in v]
    return np.matmul(np.matmul(u,np.diag(v)),u.T)

def pre_whiten(mean):
    U,s,V = np.linalg.svd(mean)
    S = np.matrix([[0 if i!=j else s[j] for j in xrange(n)] for i in xrange(n) ]) # since s contains only eigen values make it diagonal
    for j in range(0,n):
	S[j,j]=1.0/math.sqrt(S[j,j]) 
    x = np.matmul(np.matmul(U,S),V)
    output = []
    for i in samples:
	output.append(np.matmul(np.matmul(x,i),x))
    output = np.array(output)
    #print output.mean(0)
    #print np.matmul(np.matmul(x,mean),x)
    return output
        

def euclidean():
    # Initialize W such that W.T*W = I , where W is n*p matrix
    W = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(n) ])
    # return
    Y = sum(samples)/len(samples) # Frechet mean
    var = variance(W)
    for i in range(100):
        H = np.matrix([[0.0 for j in xrange(p)] for i in xrange(n) ])    
        for X in samples:
            t1 = np.matmul(X-Y,W)
            t2 = np.matmul(W.T,(X-Y))
            H += 4*(np.matmul(np.matmul(t1,t2),W))
        # H/=float(numsamples)
        W = update(W,H)
    var = variance(W)

def log_euclidean():
    # Initialize W such that W.T*W = I , where W is n*p matrix
    W = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(n) ])
    
    # Frechet mean
    mul = samples[0]
    for i in samples[1:]:
        mul = np.matmul(mul,i)
    U,s,V = np.linalg.svd(mul)
    Y = np.matrix([[0 if i!=j else s[j]**(1/(1.0*len(samples))) for j in xrange(n)] for i in xrange(n) ]) 
    
    for i in range(100):
        H = np.matrix([[0.0 for j in xrange(p)] for i in xrange(n) ])    
        for X in samples:
            h1 = matrix_log(np.matmul(np.matmul(W.T,X),W)) - matrix_log(np.matmul(np.matmul(W.T,Y),W))
            h2 = -h1
            t1 = np.matmul(np.matmul(X,W),"""some D of h1""")
            t2 = np.matmul(np.matmul(Y,W),"""some D of h2""") 
            H += 4*(t1+t2)
        # H/=float(numsamples)
        W = update(W,H)
    var = variance(W)

def airm():
    # Initialize W such that W.T*W = I , where W is n*p matrix
    W = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(n) ])

    # Frechet mean
    mul = samples[0]
    for i in samples[1:]:
        mul = np.matmul(mul,i)
    U,s,V = np.linalg.svd(mul)
    Y = np.matrix([[0 if i!=j else s[j]**(1/(1.0*len(samples))) for j in xrange(n)] for i in xrange(n) ]) 

    var = variance(W)
    for i in range(100):
        H = np.matrix([[0.0 for j in xrange(p)] for i in xrange(n) ])    
        for X in samples:
            new_x = np.matmul(np.matmul(W.T,X),W)
            new_y = np.matmul(np.matmul(W.T,Y),W)
            log_term = matrix_log(np.matmul(new_x,np.linalg.inv(new_y)))
            temp = np.matmul(np.matmul(X,W),np.linalg.inv(new_x)) - np.matmul(np.matmul(Y,W),np.linalg.inv(new_y))
            H += np.matmul(temp,log_term)
        # H/=float(numsamples)
        W = update(W,H)
    var = variance(W)    

def log_determinant():
    # Initialize W such that W.T*W = I , where W is n*p matrix
    W = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(n) ])
    
    # Frechet mean
    mul = samples[0]
    for i in samples[1:]:
        mul = np.matmul(mul,i)
    U,s,V = np.linalg.svd(mul)
    Y = np.matrix([[0 if i!=j else s[j]**(1/(1.0*len(samples))) for j in xrange(n)] for i in xrange(n) ]) 
    var = variance(W)
    
    for i in range(100):
	H = np.matrix([[0.0 for j in xrange(p)] for i in xrange(n) ])    
	for X in samples:
	    t1 = np.matmul(np.matmul((X+Y),W),np.linalg.inv(np.matmul(np.matmul(W.T,(X+Y)/2),W)))
	    t2 = np.matmul(np.matmul(X,W),np.linalg.inv(np.matmul(np.matmul(W.T,X),W)))
	    t3 = np.matmul(np.matmul(Y,W),np.linalg.inv(np.matmul(np.matmul(W.T,Y),W)))
	    H += t1 - t2 - t3
	    # H/=float(numsamples)
	    W = update(W,H)
    var = variance(W)


euclidean()
airm()
log_determinant()

