#! /usr/bin/env python

import pickle
import numpy
import numpy as np
import math
from numpy import linalg as LA
from numpy import matmul as mult

samples = pickle.load(open("dataset.pickle","rb"))
numsamples = samples.shape[0]
n = samples.shape[1] # Old Dimension
p = 2 # New Dimension
I = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(p) ])

def cosine(s):
    # In expansion of cosine, I = p*p identity, A^2 = A.T*A, A^(2k) = (A^2)^k
    n,p = s.shape
    S = np.matrix([[0 if i!=j else math.cos(s[i,j]) for j in xrange(p)] for i in xrange(p) ])
    return S

def sine(s):
    # In expansion of sine A = A, A^3 = A*(A^2) = A*A.T*A, A^(2k+1) = A*((A^2)^k)
    n,p = s.shape
    S = np.matrix([[0 if i!=j else math.sin(s[i,j]) for j in xrange(p)] for i in xrange(n) ])
    return S

def log(matrix):
    v,u = np.linalg.eig(matrix)
    v = [np.log(x) for x in v]
    return np.matmul(np.matmul(u,np.diag(v)),u.T)

def to_arr(X):
    return np.squeeze(np.asarray(X))

def frechet_derivative(X,H):
    n = X.shape[0]
    Z = [[0]*n]*n
    temp = []
    for i in range(2*n):
        t = []
        if i < n:
            for x in to_arr(X[i]):
                t.append(x)
            for x in to_arr(H[i]):
                t.append(x)
        else:
            for x in range(n):
                t.append(0)
            for x in to_arr(X[i-n]):
                t.append(x)
        temp.append(t)
    A= log(temp)
    ans = []
    for i in A[0:n]:
        t = []
        for j in range(len(i)):
            if j>=n:
                t.append(i[j])
        ans.append(t)
    return ans

def mean(data,mode):
    Y = None
    if mode == "euclidean":
        Y = data.mean(0)
    elif mode == "airm":
        mul = samples[0]
        for i in samples[1:]:
            mul = np.matmul(mul,i)
        U,s,V = np.linalg.svd(mul)
        Y = np.matrix([[0 if i!=j else s[j]**(1/(1.0*len(samples))) for j in xrange(n)] for i in xrange(n)])
        Y = mult(mult(U,Y),V)
    elif mode == "log_determinant":
        mul = samples[0]
        for i in samples[1:]:
            mul = np.matmul(mul,i)
        U,s,V = np.linalg.svd(mul)
        Y = np.matrix([[0 if i!=j else s[j]**(1/(1.0*len(samples))) for j in xrange(n)] for i in xrange(n) ]) 
    elif mode=="log_euclidean":
        mul = samples[0]
        for i in samples[1:]:
            mul = np.matmul(mul,i)
        U,s,V = np.linalg.svd(mul)
        Y = np.matrix([[0 if i!=j else s[j]**(1/(1.0*len(samples))) for j in xrange(n)] for i in xrange(n) ]) 
    return Y
    """
    fill remaining
    """

def dist(X,Y,mode):
    dist = None
    if mode == "euclidean":
        dist = (LA.norm(X-Y,None))**2
    elif mode== "airm":
        dist = (LA.norm(mult(LA.inv(X),Y),None))**2
    elif mode=="log_determinant":
        dist = np.log(LA.det((X+Y)/2) - np.log(LA.det(mult(X,Y))))/2
    elif mode=="log_euclidean":
        dist = (LA.norm(log(X)-log(Y),None))**2
    return dist

def egrad(X,Y,W,mode):
    egrad = None
    if mode == "euclidean":
        part1 = mult(X-Y,W)
        part2 = mult(W.T,(X-Y))
        egrad = 4*(mult(mult(part1,part2),W))
    elif mode== "airm":
        new_x = np.matmul(np.matmul(W.T,X),W)
        new_y = np.matmul(np.matmul(W.T,Y),W)
        log_term = log(np.matmul(new_x,np.linalg.inv(new_y)))
        temp = np.matmul(np.matmul(X,W),np.linalg.inv(new_x)) - np.matmul(np.matmul(Y,W),np.linalg.inv(new_y))
        egrad = np.matmul(temp,log_term)
    elif mode=="log_determinant":
        t1 = np.matmul(np.matmul((X+Y),W),np.linalg.inv(np.matmul(np.matmul(W.T,(X+Y)/2),W)))
        t2 = np.matmul(np.matmul(X,W),np.linalg.inv(np.matmul(np.matmul(W.T,X),W)))
        t3 = np.matmul(np.matmul(Y,W),np.linalg.inv(np.matmul(np.matmul(W.T,Y),W)))
        egrad = t1 - t2 - t3
    elif mode=="log_euclidean":
	r1 = mult(mult(W.T,X),W) 
        r2 = mult(mult(W.T,Y),W) 
        h1 = log(r1)-log(r2)
        h2 = -h1
        t1 = mult(mult(X,W),frechet_derivative(r1,h1))
        t2 = mult(mult(Y,W),frechet_derivative(r2,h2)) 
        egrad = 4*(t1+t2)
    return egrad

def rgrad(dwf,W):
    return dwf - mult(mult(W,W.T),dwf)

def neweigvals(data,W):
    ret = []
    for X in data:
        newmat = mult(W.T,mult(X,W))
        eigvals = LA.eigvalsh(newmat)
        ret.append(sum(eigvals))
    return ret

def variance(data,mode,W=None):
    curmean = mean(data,mode)
    normal = W is None
    var = 0
    for i in data:
        if normal:
            var += dist(i,curmean,mode)
        else:
            var += dist(mult(W.T,mult(i,W)),mult(W.T,mult(curmean,W)),mode)
    var/=float(data.shape[0])
    return var

def update(W,H,data,mode):
    U,s,V = np.linalg.svd(H)
    S = np.matrix([[0 if i!=j else s[j] for j in xrange(p)] for i in xrange(n) ]) # since s contains only eigen values make it diagonal
    step = 0.1
    t = 0
    oldvar = variance(data,mode,W)
    while True:
        #print t
        t += step
        newW = mult(mult(W,V.T),mult(cosine(S*t),V)) + mult(U,mult(sine(S*t),V))
        newvar = variance(data,mode,newW)
        if newvar>oldvar:
            break
    #print t
    return newW

def GAPCA(data,mode):
    W = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(n) ])
    Y = mean(data,mode)
    varp = variance(data,mode)
    topp = []
    for X in data:
        eigvals = list(LA.eigvalsh(X))
        eigvals.sort(reverse=True)
        topp.append(sum(eigvals[:p]))

    for iteration in xrange(20):
        H = np.matrix([[0.0 for j in xrange(p)] for i in xrange(n) ])    
        for X in data:
            H += egrad(X,Y,W,mode)
        H = rgrad(H,W)
        W = update(W,H,data,mode)
    	varn = variance(data,mode,W)
    	print "For Iteration",iteration+1,"variance = %f, ratio=%f"%(varn,varn/varp)
        newtopp = neweigvals(data,W)
        print "ratio =",sum(newtopp)/float(sum(topp))
    print "Compressed Variance = %f"%(varn/varp)
    return W

W = GAPCA(samples,"euclidean")
exit(0)

def log_euclidean():
    # Initialize W such that W.T*W = I , where W is n*p matrix
    W = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(n) ])
    
    # Frechet mean
    # copied to above function
    for i in range(1000):
        H = np.matrix([[0.0 for j in xrange(p)] for i in xrange(n) ])    
        for X in samples:
            h1 = log(np.matmul(np.matmul(W.T,X),W)) - log(np.matmul(np.matmul(W.T,Y),W))
            h2 = -h1
            t1 = np.matmul(np.matmul(X,W),"""some D of h1""")
            t2 = np.matmul(np.matmul(Y,W),"""some D of h2""") 
            H += 4*(t1+t2)
        # H/=float(numsamples)
        H = H - np.matmul(np.matmul(W,W.T),H)
        W = update(W,H)
    var = variance(W)


exit(0)
def pre_whiten(mean):
    U,s,V = np.linalg.svd(mean)
    S = np.matrix([[0 if i!=j else s[j] for j in xrange(n)] for i in xrange(n) ]) # since s contains only eigen values make it diagonal
    for j in range(0,n):
	S[j,j]=1.0/math.sqrt(S[j,j]) 
    x = np.matmul(np.matmul(U,S),V)
    output = []
    for i in samples:
	output.append(np.matmul(np.matmul(x,i),x))
    output = np.array(output)
    #print output.mean(0)
    #print np.matmul(np.matmul(x,mean),x)
    return output

