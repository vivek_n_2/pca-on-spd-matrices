#! /usr/bin/env python

import pickle
import numpy
import numpy as np
import math
from numpy import linalg as LA
from numpy import matmul as mult

samples = pickle.load(open("dataset.pickle","rb"))
numsamples = samples.shape[0]
n = samples.shape[1] # Old Dimension
p = 2 # New Dimension
I = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(p) ])

def cosine(s):
    # In expansion of cosine, I = p*p identity, A^2 = A.T*A, A^(2k) = (A^2)^k
    n,p = s.shape
    S = np.matrix([[0 if i!=j else math.cos(s[i,j]) for j in xrange(p)] for i in xrange(p) ])
    return S

def sine(s):
    # In expansion of sine A = A, A^3 = A*(A^2) = A*A.T*A, A^(2k+1) = A*((A^2)^k)
    n,p = s.shape
    S = np.matrix([[0 if i!=j else math.sin(s[i,j]) for j in xrange(p)] for i in xrange(n) ])
    return S

def log(matrix):
    v,u = np.linalg.eig(matrix)
    v = [np.log(x) for x in v]
    return mult(mult(u,np.diag(v)),u.T)

def to_arr(X):
    return np.squeeze(np.asarray(X))

def frechet_derivative(X,H):
    return np.log(np.absolute(H))
    """
    n = X.shape[0]
    tot = np.matrix([[0.0 for i in xrange(2*n)] for j in xrange(2*n)])
    tot[:n,:n] = X
    tot[:n,n:] = H
    #tot[n:,:n] = zeroes
    tot[n:,n:] = X
    tot = log(tot)
    D = tot[:n,n:]
    return D
    """

"""
def frechet_derivative(X,H):
    n = X.shape[0]
    temp = []
    for i in range(2*n):
        t = []
        if i < n:
            for x in to_arr(X[i]):
                t.append(x)
            for x in to_arr(H[i]):
                t.append(x)
        else:
            for x in range(n):
                t.append(0)
            for x in to_arr(X[i-n]):
                t.append(x)
        temp.append(t)
    A= log(temp)
    ans = []
    for i in A[0:n]:
        t = []
        for j in range(len(i)):
            if j>=n:
                t.append(i[j])
        ans.append(t)
    return ans
"""

def geomean(data):
    def geoutil(left,right,power,nroot):
        if left == right:
            ub,sb,vb = LA.svd(data[left])
            nb = len(sb)
            ret = mult(ub,mult(np.matrix([[0 if i!=j else sb[j]**(power/float(nroot)) for j in xrange(nb)] for i in xrange(nb)]),vb))
            return ret
        mid = (left+right)/2
        leftmat = geoutil(left,mid,power*2,nroot)
        rightmat = geoutil(mid+1,right,power*2,nroot)
        u1,s1,v1 = LA.svd(leftmat)
        n1 = len(s1)
        Apos = mult(u1,mult(np.matrix([[0 if i!=j else s1[j]**(0.5) for j in xrange(n1)] for i in xrange(n1)]),v1))
        Aneg = mult(u1,mult(np.matrix([[0 if i!=j else s1[j]**(-0.5) for j in xrange(n1)] for i in xrange(n1)]),v1))
        midt = mult(Aneg,mult(rightmat,Aneg))
        u2,s2,v2 = LA.svd(midt)
        midtnew = mult(u2,mult(np.matrix([[0 if i!=j else s2[j]**(0.5) for j in xrange(n1)] for i in xrange(n1)]),v2))
        return mult(Apos,mult(midtnew,Apos))

    N = data.shape[0]
    return geoutil(0,N-1,1,N)

def mean(data,mode):
    Y = None
    if mode == "euclidean":
        Y = data.mean(0)
    elif mode == "airm":
        Y = geomean(data)
    elif mode == "log_determinant":
        Y = geomean(data)
    elif mode=="log_euclidean":
        Y = geomean(data)
    return Y

def dist(X,Y,mode):
    dist = None
    if mode == "euclidean":
        dist = (LA.norm(X-Y,None))**2
    elif mode== "airm":
        dist = (LA.norm(log(mult(LA.inv(X),Y)),None))**2
    elif mode=="log_determinant":
        dist = np.log(LA.det((X+Y)/2)) - (np.log(LA.det(mult(X,Y)))/2.0)
    elif mode=="log_euclidean":
        dist = (LA.norm(log(X)-log(Y),None))**2
    return dist

def egrad(X,Y,W,mode):
    egrad = None
    if mode == "euclidean":
        part1 = mult(X-Y,W)
        part2 = mult(W.T,(X-Y))
        egrad = 4*(mult(mult(part1,part2),W))
    elif mode== "airm":
        new_x = np.matmul(np.matmul(W.T,X),W)
        new_y = np.matmul(np.matmul(W.T,Y),W)
        log_term = log(np.matmul(new_x,np.linalg.inv(new_y)))
        temp = np.matmul(np.matmul(X,W),np.linalg.inv(new_x)) - np.matmul(np.matmul(Y,W),np.linalg.inv(new_y))
        egrad = 4*np.matmul(temp,log_term)
    elif mode=="log_determinant":
        t1 = np.matmul(np.matmul((X+Y),W),np.linalg.inv(np.matmul(np.matmul(W.T,(X+Y)/2.0),W)))
        t2 = np.matmul(np.matmul(X,W),np.linalg.inv(np.matmul(np.matmul(W.T,X),W)))
        t3 = np.matmul(np.matmul(Y,W),np.linalg.inv(np.matmul(np.matmul(W.T,Y),W)))
        egrad = t1 - t2 - t3
    elif mode=="log_euclidean":
	r1 = mult(mult(W.T,X),W) 
        r2 = mult(mult(W.T,Y),W) 
        h1 = log(r1)-log(r2)
        h2 = -h1
        t1 = mult(mult(X,W),frechet_derivative(r1,h1))
        t2 = mult(mult(Y,W),frechet_derivative(r2,h2)) 
        egrad = 4*(t1+t2)
    return egrad

def rgrad(dwf,W):
    return dwf - mult(mult(W,W.T),dwf)

def neweigvals(data,W):
    ret = []
    for X in data:
        newmat = mult(W.T,mult(X,W))
        eigvals = LA.eigvalsh(newmat)
        ret.append(sum(eigvals))
    return ret

def variance(data,mode,W=None):
    curmean = mean(data,mode)
    normal = W is None
    var = 0
    for i in data:
        if normal:
            var += dist(i,curmean,mode)
        else:
            var += dist(mult(W.T,mult(i,W)),mult(W.T,mult(curmean,W)),mode)
    var/=float(data.shape[0])
    return var

def update(W,H,data,mode):
    U,s,V = np.linalg.svd(H)
    S = np.matrix([[0 if i!=j else s[j] for j in xrange(p)] for i in xrange(n) ]) # since s contains only eigen values make it diagonal
    step = 0.1
    t = 0
    oldvar = variance(data,mode,W)
    while t<1.0:
        t += step
        newW = mult(mult(W,V.T),mult(cosine(S*t),V)) + mult(U,mult(sine(S*t),V))
        newvar = variance(data,mode,newW)
        if newvar>oldvar:
            return newW
    return None

def GAPCA(data,mode):
    W = np.matrix([[0 if i!=j else 1 for j in xrange(p)] for i in xrange(n) ])
    Y = mean(data,mode)
    varp = variance(data,mode)
    topp = []
    for X in data:
        eigvals = list(LA.eigvalsh(X))
        eigvals.sort(reverse=True)
        topp.append(sum(eigvals[:p]))
    
    iteration = 1 
    while True:
        H = np.matrix([[0.0 for j in xrange(p)] for i in xrange(n) ])    
        for X in data:
            H += egrad(X,Y,W,mode)
        H = rgrad(H,W)
        ret = update(W,H,data,mode)
        if ret is not None:
            W = ret
        else:
            break
    	varn = variance(data,mode,W)
        newtopp = neweigvals(data,W)
        eigratios = [i/float(j) for i,j in zip(newtopp,topp)]
        eigratio = sum(eigratios)/float(len(eigratios))
        print "For Iteration",iteration,"variance = %f, ratio=%f ,eigenratio = %f"%(varn,varn/varp,eigratio)
        iteration += 1
    print "Compressed Variance = %f, compressed eigvals = %f"%(varn/varp,eigratio)
    return W

W = GAPCA(samples,"euclidean")
print "="*20
W = GAPCA(samples,"airm")
print "="*20
W = GAPCA(samples,"log_determinant")
print "="*20
W = GAPCA(samples,"log_euclidean")
print "="*20
exit(0)

def pre_whiten(mean):
    U,s,V = np.linalg.svd(mean)
    S = np.matrix([[0 if i!=j else s[j] for j in xrange(n)] for i in xrange(n) ]) # since s contains only eigen values make it diagonal
    for j in range(0,n):
	S[j,j]=1.0/math.sqrt(S[j,j]) 
    x = np.matmul(np.matmul(U,S),V)
    output = []
    for i in samples:
	output.append(np.matmul(np.matmul(x,i),x))
    output = np.array(output)
    #print output.mean(0)
    #print np.matmul(np.matmul(x,mean),x)
    return output
