#! /usr/bin/env python

from sklearn.datasets import make_spd_matrix as msm
import pickle
import numpy

dim = 17
numsamples = 50
samples = []
for i in xrange(numsamples):
    samples.append(numpy.array(msm(dim)))
samples = numpy.array(samples)

pickle.dump(samples,open("dataset.pickle", "wb"))
